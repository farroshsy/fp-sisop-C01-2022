#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <errno.h>
#include <syslog.h>

#define PORT 8080

char folder[]= "Database";

typedef struct {
    unsigned int key;
    char fname[100];
    char lname[100];
    unsigned int age;
} person_rec;

struct user
{
    char id[1000];
    char pw[1000];
}user;

int socket_cb(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read( new_socket , buffer, 1024);
    printf("%s\n",buffer );
    send(new_socket , hello , strlen(hello) , 0 );
    printf("Hello message sent\n");
    return 0;
}

void create_file(char namefile[], char str[], char mode[]){
    FILE* file = fopen(namefile, mode);
    fprintf(file, "%s/%s", str);
    fclose(file);
}

char* create_database (char str[]){
    char* pointer;
    char pesan[2048];

    char buatdb[2048];
    bzero(buatdb, 2048);

    int a;
    char parsing[2048];
    strcpy(parsing, pointer);

    char* operasi = parsing;
    char* token;

    for (a = 0; token = strtok_r(operasi, " ", &operasi); a++){
        if (a==2){
        strncpy(buatdb, token, strlen(token)-1);
        }

    }
    
    char pathdb[2048];
    sprintf(pathdb, "%s/%s", folder, buatdb);
    char* dbpathptr = pathdb;

    if (mkdir(dbpathptr, 0777)!=0){
        strcpy(pesan, "Cannot Create database!");
        pointer = pesan;
        return pointer;
    }
    char izinfile[2048];
    strcpy(izinfile, pathdb);
    strcat(izinfile, "/granteduser.txt");

    create_file(izinfile, user.id, "a");
    strcpy(pesan, "Database Successfully Created!");
    pointer = pesan;
    return pointer;
}


int main(int argc, char *argv[]){
    int fd;
    person_rec rec;

    fd = open_record("data1");

    if(argc > 1){

        if(argc > 5 && !strcmp(argv[1], "insert")){
            rec.key =atoi(argv[2]);
            strcpy(rec.fname, argv[3]);
            strcpy(rec.lname, argv[4]);
            rec.age = atoi(argv[5]);
            insert_record(fd, &rec);
        }
        if(argc > 2 && !strcmp(argv[1], "delete"))
            delete_record(fd, atoi(argv[2]));

        if(argc > 2 && !strcmp(argv[1], "print")){
            get_record(fd, &rec, atoi(argv[2]));

            printf("key = %d\n", rec.key);
            printf("First = %s\n", rec.fname);
            printf("Last = %s\n", rec.lname);
            printf("Age = %d\n", rec.age);
        }
    }
    close_record(fd);
    return 0;
}
